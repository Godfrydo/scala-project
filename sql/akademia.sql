-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Czas generowania: 08 Cze 2015, 16:02
-- Wersja serwera: 5.6.24
-- Wersja PHP: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza danych: `akademia`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kierunki`
--

CREATE TABLE IF NOT EXISTS `kierunki` (
  `Indeks` int(5) NOT NULL,
  `Uczelnia_ID` int(5) NOT NULL,
  `Kierunek_ID` int(5) NOT NULL,
  `Punkty` int(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ascii COLLATE=ascii_bin;

--
-- Zrzut danych tabeli `kierunki`
--

INSERT INTO `kierunki` (`Indeks`, `Uczelnia_ID`, `Kierunek_ID`, `Punkty`) VALUES
(1, 1, 1, 190),
(2, 2, 1, 192),
(3, 3, 1, 90),
(4, 4, 1, 100),
(5, 5, 1, 133),
(7, 4, 2, 130),
(8, 1, 3, 60),
(9, 2, 3, 80),
(10, 3, 3, 40),
(11, 2, 4, 80),
(12, 3, 4, 86),
(13, 5, 4, 102),
(14, 2, 5, 90),
(15, 3, 5, 50),
(16, 4, 5, 79),
(6, 1, 2, 150);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `spis`
--

CREATE TABLE IF NOT EXISTS `spis` (
  `Indeks` int(5) NOT NULL,
  `Nazwak` text COLLATE ascii_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ascii COLLATE=ascii_bin;

--
-- Zrzut danych tabeli `spis`
--

INSERT INTO `spis` (`Indeks`, `Nazwak`) VALUES
(1, 'Informatyka'),
(2, 'Geologia'),
(3, 'Mechatronika'),
(4, 'Socjologia'),
(5, 'Psychologia');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uczelnie`
--

CREATE TABLE IF NOT EXISTS `uczelnie` (
  `Indeks` int(5) NOT NULL,
  `Nazwau` text COLLATE ascii_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ascii COLLATE=ascii_bin;

--
-- Zrzut danych tabeli `uczelnie`
--

INSERT INTO `uczelnie` (`Indeks`, `Nazwau`) VALUES
(1, 'Politechnika Warszawska'),
(2, 'Uniwersytet Warszawski'),
(3, 'Uniwersytet Miko?aja Kopernika'),
(4, 'Politechnika ??dzka'),
(5, 'Szko?a g??wna handlowa');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `kierunki`
--
ALTER TABLE `kierunki`
  ADD KEY `Uczelnia_ID` (`Uczelnia_ID`), ADD KEY `Kierunek_ID` (`Kierunek_ID`);

--
-- Indexes for table `spis`
--
ALTER TABLE `spis`
  ADD PRIMARY KEY (`Indeks`);

--
-- Indexes for table `uczelnie`
--
ALTER TABLE `uczelnie`
  ADD PRIMARY KEY (`Indeks`), ADD KEY `Indeks` (`Indeks`);

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `kierunki`
--
ALTER TABLE `kierunki`
ADD CONSTRAINT `kierunki_ibfk_1` FOREIGN KEY (`Uczelnia_ID`) REFERENCES `uczelnie` (`Indeks`),
ADD CONSTRAINT `kierunki_ibfk_2` FOREIGN KEY (`Kierunek_ID`) REFERENCES `spis` (`Indeks`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
